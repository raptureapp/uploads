/* global Vue */
import FileUpload from './components/FileUpload.vue';

Vue.component('file-upload', FileUpload);
