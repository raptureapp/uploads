<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Uploads\Controllers')
    ->prefix('dashboard')
    ->group(function () {
        Route::post('uploads', 'UploadsController@store');
    });
