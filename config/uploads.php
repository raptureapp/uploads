<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store references in the database
    |--------------------------------------------------------------------------
    |
    | This option controls whether to migrate a media table and publish file
    | references to that table upon upload.
    |
    */

    'database' => true,

];
