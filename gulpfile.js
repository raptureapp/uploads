const { setup, styles, javascript, monitor } = require('argonauts');
const { parallel, series } = require('gulp');
const { VueLoaderPlugin } = require('vue-loader');

setup({
    name: 'Uploads',
    url: 'homestead.test',
    entries: ['upload'],
    dest: 'public',
    src: 'resources/assets',
    jsExt: '{js,vue}',
    webpack: {
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    exclude: /node_modules/,
                    use: 'vue-loader',
                },
            ],
        },
        plugins: [
            new VueLoaderPlugin(),
        ],
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
            },
        },
    },
});

exports.default = series(parallel(styles, javascript), monitor);
