<?php

namespace Rapture\Uploads\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Rapture\Hooks\Facades\Hook;
use Rapture\Uploads\Events\FileUploaded;
use Rapture\Uploads\Models\Media;

class UploadsController extends Controller
{
    public function store(Request $request)
    {
        if (!$request->hasFile('upload') || !$request->file('upload')->isValid()) {
            return response()->json([
                'error' => 'Not a valid request',
            ]);
        }

        $fileName = implode('.', [
            Str::before($request->file('upload')->hashName(), '.'),
            $request->file('upload')->getClientOriginalExtension()
        ]);

        $path = $request->file('upload')->storeAs('public', $fileName);
        $fileId = Storage::url($path);

        if (config('uploads.database')) {
            $ref = Media::create([
                'original' => $request->file('upload')->getClientOriginalName(),
                'name' => $fileName,
                'path' => Storage::url($path),
                'type' => $request->file('upload')->getClientOriginalExtension(),
            ]);

            Hook::dispatch('file.uploaded', new FileUploaded($ref));

            $fileId = $ref->id;
        }

        return response()->json([
            'id' => $fileId,
            'original' => $request->file('upload')->getClientOriginalName(),
            'name' => $fileName,
            'path' => Storage::url($path),
            'type' => $request->file('upload')->getClientOriginalExtension(),
            'size' => Storage::size($path),
            'modified' => Storage::lastModified($path),
        ]);
    }
}
