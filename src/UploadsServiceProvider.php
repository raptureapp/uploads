<?php

namespace Rapture\Uploads;

use Illuminate\Support\ServiceProvider;
use Rapture\Core\Facades\Resource;

class UploadsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'uploads');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'uploads');

        if (config('uploads.database')) {
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../public' => public_path('uploads'),
            ], 'rapture');

            $this->publishes([
                __DIR__ . '/../config/uploads.php' => config_path('uploads.php'),
            ], 'config');
        }

        Resource::register('file-upload', [
            'styles' => ['uploads/css/upload.css'],
            'components' => ['uploads/js/upload.js'],
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/uploads.php', 'uploads');
    }
}
