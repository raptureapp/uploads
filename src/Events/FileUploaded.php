<?php

namespace Rapture\Uploads\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rapture\Uploads\Models\Media;

class FileUploaded
{
    use Dispatchable, SerializesModels;

    public $media;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }
}
