# Upload

Routes and Vue component.

## Requirements

* `csrf-token` meta tag

## Installation



## Properties

* id: (String; defaults to a random string)
* files: (Array)
* file: (String)
* extensions (Object)
* multiple (Boolean; defaults to false)
* name (String; defaults to 'upload' when a single file and 'upload[]' for multiple files)
* url (String; defaults to '/dashboard/uploads')

## Methods

* Clear uploaded files: `eventbus.$emit('file-upload:{componentID}:clear')`
    * Please note this doesn't actually remove the files from the server

## Events

To interact with events add a listener on the event bus for `file-upload:{componentID}` followed by an event listed below.

* `cleared`
* `uploaded`
* `removed`
